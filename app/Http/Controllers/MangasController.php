<?php

namespace App\Http\Controllers;

use App\Models\Mangas;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MangasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mangas = Mangas::all();
        return Inertia::render('Manga', [
            'mangas' => $mangas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Addmanga');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:655',
            'image' => 'required',
            'chapters' => 'required',
            'year' => 'required',
        ]);
        Mangas::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $request->image,
            'chapters' => $request->chapters,
            'year' => $request->year,
        ]);
        return redirect(route('mangas.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(mangas $mangas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(mangas $mangas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, mangas $mangas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(mangas $mangas)
    {
        //
    }
}
