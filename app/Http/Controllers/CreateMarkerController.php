<?php

namespace App\Http\Controllers;

use App\Models\create_marker;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CreateMarkerController extends Controller
{
    public function index()
    {
        return Inertia::render('Map', [
            'markers' => create_marker::all(),
        ]);
    }

    public function store(Request $request)
    {
        create_marker::create($request->validate([
            'name' => ['required'],
            'lat' => ['required'],
            'lng' => ['required'],
            'description' => ['required'],
        ]));

        return redirect(to:route(back()));
    }

    public function update(Request $request, create_marker $create_marker)
    {
    }

    public function destroy(create_marker $create_marker)
    {
    }
}
